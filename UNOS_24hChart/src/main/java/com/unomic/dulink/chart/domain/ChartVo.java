
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String chartStatus;
	String OperationTime;
	int offset;
	int maxRow;
	String sDate;
	String eDate;
	String status;
	String startDateTime;
	String endDateTime;
	String spdLoad;
	String feedOverride;
	String workDate;
	int timeDiff;
	
	//common
	String ty;
	String fileLocation;
	String url;
	String fileName;
	String appId;
	String appName;
	String categoryId;
	Integer shopId;
	Integer dvcId;
	String id;
	String pwd;
	String name;
	String msg;
	String rgb;
	
	String type;
	
}
