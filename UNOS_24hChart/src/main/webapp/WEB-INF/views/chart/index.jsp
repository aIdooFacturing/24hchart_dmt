<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<%-- <script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script> --%>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-rotate.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	
	function getStartTime(){
		var url = ctxPath + "/getStartTime.do";
		var param = "shopId=" + shopId;;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				startHour = data.split(":")[0]
				startMinute = data.split(":")[1]

				//drawBarChart("timeChart");
				
				var date = new Date();
				var year = date.getFullYear();
				var month = addZero(String(date.getMonth() + 1));
				var day = addZero(String(date.getDate()));
				var hour = date.getHours();
				var minute = addZero(String(date.getMinutes())).substr(0,1);
				
				
				if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
					day = addZero(String(new Date().getDate()+1));
				};
				
				
				var today = year + "-" + month + "-" + day;
				
				
				getDvcIdList(today);
			}, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});	
	};
	
	function changeDateVal(){
			console.log("changed")
			var now = new Date(); 
			var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
			
			var date = new Date();
			
			var year = date.getFullYear();
			var month = date.getMonth()+1;
			var day = date.getDate();
			var hour = addZero(String(date.getHours()));
			var minute = addZero(String(date.getMinutes()));
			var second = addZero(String(date.getSeconds()));
			
			// Set specificDate to a specified date at midnight.
			
			var selectedDate = $("#today").val();
			var s_year = selectedDate.substr(0,4);
			var s_month = selectedDate.substr(5,2);
			var s_day = selectedDate.substr(8,2);

			var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
			
			var time = year + "-" + month + "-" + day;
			
			var today = getToday().substr(0,10);
			if(todayAtMidn.getTime()<specificDate.getTime()){
				alert("오늘 이후의 날짜는 선택할 수 없습니다.");
				$("#today").val(today);
				return;
			};
			
			
			if(today==$("#today").val()){
				var date = new Date();
				var year = date.getFullYear();
				var month = addZero(String(date.getMonth() + 1));
				var day = addZero(String(date.getDate()));
				var hour = date.getHours();
				var minute = addZero(String(date.getMinutes())).substr(0,1);
				
				
				if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
					day = addZero(String(new Date().getDate()+1));
				};
				
				
				selectedDate = year + "-" + month + "-" + day;
				
				
			}
			
			getDvcIdList(selectedDate)
			//drawBarChart("timeChart", selectedDate);

		};
		
		function time(){
			$("#time").html(getToday());
			 handle = requestAnimationFrame(time)
		};
		
		function upDate(){
			var $date = $("#today").val();
			var year = $date.substr(0,4);
			var month = $date.substr(5,2);
			var day = $date.substr(8,2);
			
			var current_day = month + "/" + day + "/" + year;
			
			var date = new Date(current_day);
			date.setDate(date.getDate() + 1);
			
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth()+1));
			var day = addZero(String(date.getDate()));
			
			var today = year + "-" + month + "-" + day;
			$("#today").val(today);
			changeDateVal();
		};
		
		function downDate(){
			var $date = $("#today").val();
			var year = $date.substr(0,4);
			var month = $date.substr(5,2);
			var day = $date.substr(8,2);
			
			var current_day = month + "/" + day + "/" + year;
			
			var date = new Date(current_day);
			date.setDate(date.getDate() - 1);
			
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth()+1));
			var day = addZero(String(date.getDate()));
			
			var today = year + "-" + month + "-" + day;
			$("#today").val(today);
			changeDateVal();
		};
		
		
	$(function(){
		//getAppList();
		
		$("#up").click(upDate);
		$("#down").click(downDate);
		
		$("#today").change(changeDateVal);
		$("#next").click(nextPage)
		$("#prev").click(prevPage)
		
		setStartTime();
		createNav("monitor_nav", 3);
		
		setEl();
		getStartTime()
		setDate();	
		time();
		
		setEvt();
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			//"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#date_table").css({
			"position" : "absolute",
			"top" :getElSize(200),
			"right" : getElSize(50)
		});
		
		$("#today").val(getToday().substr(0,10)).css({
			"font-size" : getElSize(30) + "px",
			"height" : getElSize(60)
		})
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		
		$(".status").css({
			"width" : getElSize(1600),
			"height" : getElSize(170),
			"margin-bottom": -contentHeight/(targetHeight/10)	
		});
		
		$("#prev, #next").css({
			"width" : getElSize(100),
			"margin" : getElSize(50),
			"background-color" : "white",
			"border-radius" : "50%",
			"cursor" : "pointer"
		});
		
		$("img").css({
			"display" : "inline"
		});
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#delDiv").css({
			"color" : "white",
			"z-index" : 9999999,
			"background-color" : "black",
			"position" : "absolute",
			"width" : getElSize(700) + "px",
			"font-size" : getElSize(60) + "px",
			"text-align" : "center",
			"padding" : getElSize(30) + "px",
			"border" : getElSize(7) + "px solid rgb(34,34,34)",
			"border-radius" : getElSize(50) + "px"
		});
		
		$("#delDiv div").css({
			"background-color" : "rgb(34,34,34)",
			"padding" : getElSize(20) + "px",
			"margin" : getElSize(10) + "px",
			"cursor" : "pointer"
		}).hover(function(){
			$(this).css({
				"background-color" : "white",
				"color" : "rgb(34,34,34)",
			})
		}, function(){
			$(this).css({
				"background-color" : "rgb(34,34,34)",
				"color" : "white",
			})
		});
		
		$("#delDiv").css({
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"top" : - $("#delDiv").height() * 2
		});
		
		$("#delDiv div:nth(1)").click(function(){
			$("#delDiv").animate({
				"top" : - $("#delDiv").height() * 2
			});
		});
		
		$("#up, #down").css({
			"width" : getElSize(70) *  1.2,
			"height" : getElSize(70),		
		})
		
		if(getParameterByName('lang')=='ko'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='cn'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='de'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getDvcIdList(today){
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));

		var sDate = year + "-" + month + "-" + day;
		var eDate = year + "-" + month + "-" + day + " 23:59:59"; 
		
		var param = "sDate=" + $("#today").val() + 
					"&shopId=" + shopId + 
					"&maxRow=" + max_row + 
					"&offset=" + ((c_page-1)*max_row);
		
		var url = "${ctxPath}/getBarChartDvcId.do";
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$(".status").empty();
				
				var json = data.dvcId;
				
				if(json.length>=max_row){
					$("#next").addClass("enablePointer");
				}else{
					$("#next").removeClass("enablePointer");
				}
				
				for(var i = 0; i < json.length; i++){
					//drawBarChart2("status2_" + i, json[i].name, today);	
				    $("#status2_" + i).attr("dvcId", json[i].dvcId);
					$("#status2_" + i).attr("name", json[i].name);
					$("#status2_" + i).dblclick(function(){goSingleChart(this)});
					getStatusChart2(json[i].dvcId, i, today, json[i].name, json[i].type);
				}
			}
		});
	};
	
	function goSingleChart(el){
		var dvcId = $(el).attr("dvcId");
		var name = $(el).attr("name");
		var date = $("#today").val();
		
		window.sessionStorage.setItem("dvcId", dvcId);
		window.sessionStorage.setItem("name", name);
		window.sessionStorage.setItem("date", date);
		
		window.localStorage.setItem("dvcId",dvcId);
		location.href="/Single_Chart_Status/index.do";
		
	};
	
	var c_page = 1;
	var max_row = 20;
	
	var incycleColor = "#50BA29";
	var cuttingColor = "#175501";
	var waitColor = "yellow";
	var alarmColor = "red";
	var noconnColor= "#A0A0A0";
	
	function getStatusChart2(dvcId, idx, today, name, type){
		
		var endDateTime=$("#today").val() +' '+ startHour +':' +startMinute;
		var startDateTime=moment($("#today").val()).subtract(1, 'd').format("YYYY-MM-DD") +' ' + startHour +':' +startMinute
		
		/* if(moment(endDateTime).isSameOrAfter(moment())){
			console.log("같은날자 입니다.")			
		}else{
			startDateTime = moment(startDateTime).add(1,'d').format("YYYY-MM-DD HH:mm")
			endDateTime = moment(endDateTime).add(1,'d').format("YYYY-MM-DD HH:mm")
		} */
		
		var url = "${ctxPath}/getTimeData.do";
		var param = "startDateTime=" + startDateTime + 
		"&endDateTime=" + endDateTime +
		"&dvcId=" + dvcId;
		
		/* setInterval(function (){
			var minute = String(new Date().getMinutes());
			if(minute.length!=1){
				minute = minute.substr(1,2);
			};
			
			if(minute==2 && eval("dvcMap" + idx).get("initFlag") || minute==2 && typeof(eval("dvcMap" + idx).get("initFlag")=="undefined")){
				//getStatusChart2(dvcId, idx, today);
				console.log("init")
				eval("dvcMap" + idx).put("initFlag", false);
				eval("dvcMap" + idx).put("currentFlag", true);
			}else if(minute!=2){
				eval("dvcMap" + idx).put("initFlag", true);
			};
		}, 1000 * 10); */
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				var json = data.statusList;
				
				console.log(json)
				
				var color = "";
				var f_Hour;
				var f_Minute;
				var f_Second;
				if(json.length>0){
					f_Hour = json[0].startDateTime.substr(11,2);
					f_Minute = json[0].startDateTime.substr(14,2);
					f_Second = json[0].startDateTime.substr(17,2);
				}else{
					f_Hour = moment().hour();
					f_Minute = moment().minute();
					f_Second = moment().seconds();
				}
				
				var startN = 0;
				
				var pushArray=[];
				
				if(Number(f_Hour)==Number(startHour) && Number(f_Minute)==(Number(startMinute))){
					
				}else{
					if(f_Hour>=Number(startHour)){
						startN = (((f_Hour*60*60) + Number(f_Minute*60)+Number(f_Second)) - ((startHour*60*60) + (startMinute*60))); 
					}else{
						startN = ((24*60*60) - ((startHour*60*60) + (startMinute*60)));
						startN += (f_Hour*60*60) + (f_Minute*60);
					};
					pushArray.push({
						data : [startN],
						color: "gray"
					})
				};
				
				$(json).each(function(idx, data){
					
					
						if(data.status=="IN-CYCLE"){
							color = incycleColor;
							if(type=='IOL'){
								color = cuttingColor;
							}
						}else if(data.status=="WAIT"){
							color = waitColor;
						}else if(data.status=="ALARM"){
							color = alarmColor;
						}else if(data.status=="NO-CONNECTION"){
							color = noconnColor;
						}else if(data.status=="CUT"){
							color = cuttingColor;
						}else{
							color = "purple"								
						}
						pushArray.push({
							data : [data.diff],
							color: color,
						})
				});
				
				
				var insertArray = [];
				
				for(var i=pushArray.length-1;i>-1;i--){
					insertArray.push({
						data : pushArray[i].data,	
						color: pushArray[i].color,
					});	
				}
				
				
				Highcharts.chart("status2_"+idx, {
					    chart: {
					        type: 'bar',
					        backgroundColor : 'rgba(0,0,0,0)',
					        height : getElSize(170),
					        marginRight : getElSize(50),
					    	marginLeft : getElSize(50),
					    	spacingTop : -getElSize(100)
					    },
					    credits : false,
						title : false,
					    tooltip: {
					        enabled: false
					    },
					    boost : {
					    	useGPUTranslations : true
					    },
					    xAxis: {
					    	labels: {
					    		style : {
									color : "rgba(0,0,0,0)",
									fontSize : getElSize(30),
									fontWeight : "bold"
								}
				            },
					    	visible: false
					    },
					    yAxis: {
					        startOnTick: true,
					        endOnTick: true,
					        gridLineWidth: 3,
					        title :{
					        	 offset : -25,
					        	 useHTML : true,
					        	 text : "<div class='dvc-title'>"+name+"</div>"
					        	 
					        },
					        tickInterval: 3600,
					        minorGridLineColor: '#4f4f4f',
					        minorTickInterval:600,
					        max:86400,
							labels : {
								step: 1,
								formatter : function() {
									var val = this.value
									if(val>0){
										val=val/3600+Number(startHour)
									}else{
										val=Number(startHour)
									}
									if(val>24){
										val-=24
									}
									if(startMinute>0){
										return val+":"+startMinute;
									}
									return val;
								},
								style : {
									color : "white",
									fontSize : getElSize(15),
									fontWeight : "bold"
								},
							},
					    },
					    legend: {
					        enabled: false
					    },
					    plotOptions: {
					        series: {
					            stacking: 'bar',
					            borderWidth: 0,
					            pointWidth:getElSize(170),
					            stickyTracking: true,
					            turboThreshold: 3000
					        },
					        line : {
								marker : {
									enabled : true
								}
							}
					    },
					    series: insertArray
				});
				
				$(".highcharts-axis-title").css({
					"left" : getElSize(80),
					"margin-top" : getElSize(-30),
					"font-size" : getElSize(50),
					"color" : "white",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				})
				
			}
		});
	};
	
	function setStartTime(){
		var url = "${ctxPath}/getStartTime.do"
		
		$.ajax({
			url :url,
			dataType :"text",
			type : "post",
			success : function(data){
				startTime = Number(data);
				for(var i = 0; i<=24; i++){
					if(i+startTime>24){
						startTime -= 24;
					};
					
					timeLabel.push(i+startTime);
					for(var j = 0; j < 5; j++){
						timeLabel.push(0);	
					};
				};					
			}
		});
	};
	
	var timeLabel = [];
	
	function slctChartColor(status){
		var color;
		if(status.toLowerCase()=="in-cycle"){
			color = colors[0];
		}else if(status.toLowerCase()=="wait"){
			color = colors[1];
		}else if(status.toLowerCase()=="alarm"){
			color = colors[2];
		}else if(status.toLowerCase()=="no-connection"){
			color = colors[3];
		};
		
		return color;
	};
	
	var startTimeLabel = new Array();
	
	var startHour;
	var startMinute;
	
	var colors;
	
	
	
	var colors;
	
	function drawBarChart2(id, name){
		var fontColor = "white;"
		if(name=="NB13" || name=="NB14W"){
			fontColor = "black";
		}
		
		var m0 = "",
		m02 = "",
		m04 = "",
		m06 = "",
		m08 = "",
		
		m1 = "";
		m12 = "",
		m14 = "",
		m16 = "",
		m18 = "",
		
		m2 = "";
		m22 = "",
		m24 = "",
		m26 = "",
		m28 = "",
		
		m3 = "";
		m32 = "",
		m34 = "",
		m36 = "",
		m38 = "",
		
		m4 = "";
		m42 = "",
		m44 = "",
		m46 = "",
		m48 = "",
		
		m5 = "";
		m52 = "",
		m54 = "",
		m56 = "",
		m58 = "";
	
	var n = Number(startHour);
	if(startMinute!=0) n+=1;
	
	for(var i = 0, j = n ; i < 24; i++, j++){
		eval("m" + Number(startMinute/10) + "=" + j);
		
		startTimeLabel.push(m0);
		startTimeLabel.push(m02);
		/* startTimeLabel.push(m04);
		startTimeLabel.push(m06);
		startTimeLabel.push(m08); */
		
		startTimeLabel.push(m1);
		startTimeLabel.push(m12);
		startTimeLabel.push(m14);
		/* startTimeLabel.push(m16);
		startTimeLabel.push(m18); */
		
		startTimeLabel.push(m2);
		startTimeLabel.push(m22);
		/* startTimeLabel.push(m24);
		startTimeLabel.push(m26);
		startTimeLabel.push(m28); */
		
		startTimeLabel.push(m3);
		startTimeLabel.push(m32);
		startTimeLabel.push(m34);
		/* startTimeLabel.push(m36);
		startTimeLabel.push(m38); */
		
		startTimeLabel.push(m4);
		startTimeLabel.push(m42);
		/* startTimeLabel.push(m44);
		startTimeLabel.push(m46);
		startTimeLabel.push(m48); */
		
		startTimeLabel.push(m5);
		startTimeLabel.push(m52);
		startTimeLabel.push(m54);
		/* startTimeLabel.push(m56);
		startTimeLabel.push(m58); */
		
		if(j==24){ j = 0}
	};
	
		
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	         
		var options = {
			chart : {
				type : 'coloredarea',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height : $("#mainTable").height()*0.9/11, 
				marginTop: -60,
				marginBottom: 25
			},
			credits : false,
			exporting: false,
			title : {
				text :name,
				align :"left",
				y:10,
				style : {
					color : "black",
					fontSize: getElSize(40) + "px",
					fontWeight: 'bold'
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false,
	                rotation: 0,
	                "textAlign": 'right',
	                x:100,
	                y: -getElSize(10),
					style : {
						color : "black",
						fontSize: getElSize(20) + "px",
						fontWeight: 'bold'
					}
				},
			},
			xAxis:{
		           categories:startTimeLabel,
		            labels:{
		            	step: 1,
						formatter : function() {
							var val = this.value

							return val;
						},
			                        style :{
			    	                	color : fontColor,
			    	                	fontSize : "9px"
			    	                },
		            }
		        },
		       
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			plotOptions: {
			    line: {
			        marker: {
			            enabled: false
			        }
			    },
			    series : {
			    	animation : false
			    }
			},
			legend : {
				enabled : false
			},
			series: []
		}

	   	$('#' + id).highcharts(options);
	};
	
	function nextPage(){
		if($("svg").length<(max_row)) return;
		c_page++;
		if(c_page>1) $("#prev").addClass("enablePointer");
		getDvcIdList($("#today").val())
	};

	var dateArray = []
	function prevPage(){
		if(c_page<=1) return;
		c_page--;
		if(c_page==1) $("#prev").removeClass("enablePointer");
		getDvcIdList($("#today").val());
	}
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}


	
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="time"></div>
	<div id="title_right"></div>
	
	<div id="dashBoard_title">${barchart_title}</div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<table id="date_table">
									<tr>
										<!-- <td>
											<button id="up">⬆︎</button><button id="down">⬇︎</button>
										</td> -->
										<td>
											<button id="up">▲</button><button id="down">▼</button>
										</td>
										<td>
											<input type="date" id="today" >		
										</td>
									</tr>
							</table>
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="mainTable" style="border-collapse: collapse; width: 100%" >
						<tr>
							<td class="leftTd">
								<div id="status2_0" class="status"></div>
							</td>
							<td>
								<div id="status2_1" class="status"></div>			
							</td>
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_2" class="status"></div>
							</td>
							<td>
								<div id="status2_3" class="status"></div>
							</td>
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_4" class="status"></div>
							</td>
							<td>
								<div id="status2_5" class="status"></div>
							</td>
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_6" class="status"></div>
							</td>
							<td>
								<div id="status2_7" class="status"></div>
							</td>
						</tr>	
						<tr>		
							<td class="leftTd">
								<div id="status2_8" class="status"></div>
							</td>
							<td>
								<div id="status2_9" class="status"></div>
							</td>
						<tr>	
							<td class="leftTd">
								<div id="status2_10" class="status"></div>
							</td>
							<td>
								<div id="status2_11" class="status"></div>
							</td>
						</tr>	
						<tr>
							<td class="leftTd">
								<div id="status2_12" class="status"></div>
							</td>
							<Td>
								<div id="status2_13" class="status"></div>
							</Td>
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_14" class="status"></div>
							</td>
							<td>
								<div id="status2_15" class="status"></div>
							</td>	
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_16" class="status"></div>
							</td>
							<td>
								<div id="status2_17" class="status"></div>
							</td>
						</tr>	
						<tr>		
							<Td class="leftTd">
								<div id="status2_18" class="status"></div>
							</Td>	
							<td>
								<div id="status2_19" class="status"></div>
							</td>
						</tr>
						<Tr>
							<td colspan="2" align="center" style="padding: 0px">
								<img alt="" src="${ctxPath }/images/arrow_left_black.png" id="prev">
								<img alt="" src="${ctxPath }/images/arrow_right_black.png" id="next">
							</td>
						</Tr>	
					</table>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	